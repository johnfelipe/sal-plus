# Login  into server with ssh

# To install Web Server 
```

# Install Apache2
sudo apt-get update
sudo apt-get install apache2
sudo a2enmod rewrite
sudo  nano  /etc/apache2/apache2.conf
change line:
AllowOverride None to AllowOverride All
sudo service apache2 restart
```
# Install  PHP5.6
```
sudo apt-get install python-software-properties
sudo add-apt-repository ppa:ondrej/php
sudo apt update
sudo apt install apache2 libapache2-mod-php5.6
sudo apt-get install -y php5.6 libapache2-mod-php5.6 php5.6-common php5.6-gd php5.6-mysql php5.6-mcrypt php5.6-curl php5.6-intl php5.6-xsl php5.6-mbstring php5.6-zip php5.6-bcmath php5.6-iconv php5.6-soap

service apache2 restart
```
# Install Mysql  server
```
sudo apt-get update
sudo apt-get install mysql-server
sudo mysql_secure_installation
sudo mysql
ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'UfdstfREfdD43453dD';
FLUSH PRIVILEGES;
You can also follow this url: https://vitux.com/how-to-install-and-configure-mysql-in-ubuntu-18-04-lts/

sudo apt-get install phpmyadmin
Create database: https://ciid-cis.dev/phpmyadmin
User:root
Password: UfdstfREfdD43453dD
```

# Setup Repository 
```
sudo apt-get install git
cd /var/www/html/
git https://github.com/luisdias/sal-plus.git

cd sal-plus

Create database name: sal
Import sql file commandline

mysql -u root sal < salplus-database.sql -p

To change mysql connection string open file

app/config/database.php


```

# Setup Virtual host copy and paste below lines 

```
 sudo nano /etc/apache2/sites-available/000-default.conf

 <VirtualHost *:80>
        # The ServerName directive sets the request scheme, hostname and port that
        # the server uses to identify itself. This is used when creating
        # redirection URLs. In the context of virtual hosts, the ServerName
        # specifies what hostname must appear in the request's Host: header to
        # match this virtual host. For the default virtual host (this file) this
        # value is not decisive as it is used as a last resort host regardless.
        # However, you must set it for any further virtual host explicitly.
        #ServerName www.example.com

        ServerAdmin webmaster@localhost
        DocumentRoot /var/www/html/sal-plus


        <Directory /var/www/html/sal-plus>
           Options -Indexes +FollowSymLinks
           AllowOverride All
        </Directory>

        # Available loglevels: trace8, ..., trace1, debug, info, notice, warn,
        # error, crit, alert, emerg.
        # It is also possible to configure the loglevel for particular
        # modules, e.g.
        #LogLevel info ssl:warn

        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined

        # For most configuration files from conf-available/, which are
        # enabled or disabled at a global level, it is possible to
        # include a line for only one particular virtual host. For example the
        # following line enables the CGI configuration for this host only
        # after it has been globally disabled with "a2disconf".
        #Include conf-available/serve-cgi-bin.conf
</VirtualHost>


sudo service apache2 restart
```

#  Install Letsencrypt

```
sudo add-apt-repository ppa:certbot/certbot
sudo apt install python-certbot-apache
sudo certbot --apache -d ciid-cis.dev

Open URL IN BRowser: https://ciid-cis.dev/

```
